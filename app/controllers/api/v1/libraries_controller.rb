class Api::V1::LibrariesController < ApplicationController
  def index
    @libraries = Library.all
    render :json => @libraries
  end

  def create
    @library = Library.new(library_params)
    if @library.save
      render :json => @library
    else
      render :json => @library.errors
    end
  end

  private
  def library_params
    params.require(:data).require(:attributes).permit(:name, :address, :phone)
  end
end
