class Api::V1::InvitationsController < ApplicationController
  def index
    @invitations = Invitation.all
    render :json => @invitations
  end

  def create
    Rails.logger.info('deserialize params: #{invitation_params}')
    @invitation = Invitation.new(invitation_params)
    if @invitation.save
      render :json => @invitation
    else
      render :json => @invitation.errors
    end
  end

  private
  def invitation_params
    params.require(:data).require(:attributes).permit(:email)
  end
end
